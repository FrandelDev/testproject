// index.test.js
const logMessage = require('./index');

test('verifica que el mensaje impreso sea Hello World', () => {
    const consoleSpy = jest.spyOn(console, 'log');
    const message = logMessage();
    
    expect(consoleSpy).toHaveBeenCalledWith("Hello World");
    expect(message).toBe("Hello World");

    consoleSpy.mockRestore();
});
